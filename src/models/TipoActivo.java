/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Jadpa28
 */
public enum TipoActivo {
    
    MUEBLES(5),
    MAQUINARIA(5),
    EQUIPO_COMPUTO(2),
    VEHICULO(5),
    EDIFICIO(10);
    
    private int vidaUtil;

    private TipoActivo(int vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    public int getVidaUtil() {
        return vidaUtil;
    }
    
}
