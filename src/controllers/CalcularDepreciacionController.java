/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import models.TipoActivo;
import static views.IFrmCalcularDepreciacion.LINEA_RECTA;
import static views.IFrmCalcularDepreciacion.SDA_ASC;
import static views.IFrmCalcularDepreciacion.SDA_DESC;

/**
 *
 * @author Jadpa28
 */
public class CalcularDepreciacionController {
    
    private String header[] = {"Años", "Depreciacion del año", "Depecion acumulada",
        "Valor residual"};
    private Object[][] data;

    public CalcularDepreciacionController() {
        this.data = null;
    }
    
    public ComboBoxModel getCmbModelTipoActivo(){
        return new DefaultComboBoxModel(TipoActivo.values());
    }
    
    public void calcularDepreciacion(int method, double valorActivo, double valorResidual, int vidaUtil) {
        Object[][] tablaDepreciacion = new Object[vidaUtil+1][header.length];
        switch(method) {
            case LINEA_RECTA: {
                double depreciacionAcumulada = 0;
                double valResidual = valorActivo;
                for(int i=0; i<=vidaUtil; i++) {
                    Object[] fila = new Object[header.length];
                    int ano = i;
                    double depreciacion;
                    if(ano == 0) {
                        depreciacion = 0;
                        depreciacionAcumulada += 0;
                    } else {
                        depreciacion = (valorActivo - valorResidual) / vidaUtil;
                        depreciacionAcumulada += depreciacion;
                        valResidual = valorActivo - depreciacionAcumulada;
                    }
                    
                    fila[0] = ano;
                    fila[1] = depreciacion;
                    fila[2] = depreciacionAcumulada;
                    fila[3] = valResidual;
                    
                    tablaDepreciacion[i] = fila;
                }
                
                setData(tablaDepreciacion);
                break;
            }
            case SDA_ASC: {
                double depreciacionAcumulada = 0;
                double valResidual = valorActivo;
                int s = (vidaUtil*(vidaUtil+1)) / 2;
                for(int i=0; i<=vidaUtil; i++) {
                    Object[] fila = new Object[header.length];
                    int ano = i;
                    double depreciacion;
                    if(ano == 0) {
                        depreciacion = 0;
                        depreciacionAcumulada += 0;
                    } else {
                        double fraccion = (double)ano / (double)s;
                        depreciacion = fraccion * (valorActivo - valorResidual);
                        depreciacionAcumulada += depreciacion;
                        valResidual = valorActivo - depreciacionAcumulada;
                    }
                    
                    fila[0] = ano;
                    fila[1] = depreciacion;
                    fila[2] = depreciacionAcumulada;
                    fila[3] = valResidual;
                    
                    tablaDepreciacion[i] = fila;
                }
                
                setData(tablaDepreciacion);
                break;
            }
            case SDA_DESC: {
                double depreciacionAcumulada = 0;
                double valResidual = valorActivo;
                int s = (vidaUtil*(vidaUtil+1)) / 2;
                for(int i=0; i<=vidaUtil; i++) {
                    Object[] fila = new Object[header.length];
                    int ano = i;
                    double depreciacion;
                    if(ano == 0) {
                        depreciacion = 0;
                        depreciacionAcumulada += 0;
                    } else {
                        double fraccion = (double)((vidaUtil+1)-ano) / (double)s;
                        System.out.println(fraccion);
                        depreciacion = fraccion * (valorActivo - valorResidual);
                        depreciacionAcumulada += depreciacion;
                        valResidual = valorActivo - depreciacionAcumulada;
                    }
                    
                    fila[0] = ano;
                    fila[1] = depreciacion;
                    fila[2] = depreciacionAcumulada;
                    fila[3] = valResidual;
                    
                    tablaDepreciacion[i] = fila;
                }
                
                setData(tablaDepreciacion);
                break;
            }
        }
    }

    public Object[][] getData() {
        return data;
    }

    public void setData(Object[][] data) {
        this.data = data;
    }
    
    public TableModel getTableModel() {
        return new DefaultTableModel(getData(), header);
    }
    
}
